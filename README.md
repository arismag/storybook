# STORYBOOK

|Framework|version|
| ---     | ---   |
| html    | 5.2.6 |

Check storybook project for updates:
- [milestones](https://github.com/storybookjs/storybook/milestones)
- [boards](https://github.com/storybookjs/storybook/projects/3)

## Structure
| Structure                   | Description                                    |
| ---                         | ---                                            |
| `designsystem/00-tokens`    | All design tokens (colors, typography, etc.)   |
| `designsystem/00-base`      | Base elements (headings lists, etc.)           |
| `designsystem/00-layouts`   | Layout patterns (grids, columns etc.)          |
| `designsystem/00-components`| Components (menus, tabs, cards, etc.)          |
| `designsystem/00-pages`     | Pages (content types, landing pages etc.)      |

- [Lock down node version](https://medium.com/@faith__ngetich/locking-down-a-project-to-a-specific-node-version-using-nvmrc-and-or-engines-e5fd19144245) with `.nvmrc` file.
- Write your stories under the `designsystem` folder.
- See existing samples to get ideas.

## HOW TO RUN STORYBOOK
- `nvm use` (every time you work on a project)
- `npm install` (first time or on npm updates)
- `npm run storybook`

## WEBPACK/STORYBOOK CONFIGURATIONS

### SASS
Add SASS compiler that compiles `.scss` files with resources and linter.

| NPM packages          |
| ---                   |
| node-sass             |
| sass-lint             |
| sass-lint-webpack     |
| sass-loader           |
| sass-resources-loader |

#### Linter
On `webpack.config.js`:

```js
// Import sass linter.
const SassLintPlugin = require('sass-lint-webpack');
```
and
```
// Add sass linter plugin.
config.plugins.push(
  new SassLintPlugin()
);
```

You also need a `.sass-lint.yml` file for linter rules.

#### SASS compiler
On `webpack.config.js`:

```js
// SASS.
config.module.rules.push({
  test: /\.scss$/,
  use: [
    'style-loader',
    {
      loader: 'css-loader',
      options: {
        sourceMap: true,
      },
    },
    {
      loader: 'sass-loader',
      options: {
        sourceMap: true,
      },
    },
    {
      loader: 'sass-resources-loader',
      options: {
        resources: path.resolve(__dirname, '../designsystem/00-tokens/**/_*.scss')
        // Or array of paths
        // resources: ['./path/to/vars.scss', './path/to/mixins.scss']
      },
    },
  ],
});
```
#### Add sass on story
On 'pattern.stories.ts' add:
```js
import './_card.scss';
```
And relative file on pattern directory. Files will compile with dependencies on sass files that are on `00-tokens` directory.

### JS
Javascript compiler. Storybook uses babel as transpiler with ES6.

| Package       |
| ---           |
| eslint        |
| eslint-loader |

#### JS Linter
On `webpack.config.js`:

```js
// Add js linter plugin.
config.module.rules.push({
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'eslint-loader',
      options: {
        emitWarning: true,
      },
    },
);
```

#### Add js on a story
On 'pattern.stories.ts' add:
```js
import './pattern.js';
```
And relative file on pattern directory.

### ADDONS
Each addon comes with specific documentation and instructions. So it's better to go directly to it's repository or npm or storybook page.

| Addons used | Links |
| ---         | ---   |
| a11y        | https://www.npmjs.com/package/@storybook/addon-a11y |
| backgrounds | https://www.npmjs.com/package/@storybook/addon-backgrounds |
| console     | https://www.npmjs.com/package/@storybook/addon-console |
| knobs       | https://github.com/storybookjs/storybook/tree/master/addons/knobs |
| viewport    | https://www.npmjs.com/package/@storybook/addon-docs |
| faker       | https://www.npmjs.com/package/faker |

### VARIATIONS
On storybook, `knobs` is the addon that gives you the possibility to check patterns variations.

Combined with `faker` you can also have random content.

On `config.js`:
```
import { withKnobs } from '@storybook/addon-knobs/html';
```
On `pattern.stories.ts`:
```js
import { storiesOf } from '@storybook/html';
import { boolean, text } from '@storybook/addon-knobs/html'; // Available knobs: https://www.npmjs.com/package/@storybook/addon-knobs#available-knobs
import Faker from 'faker';

import CardWithVariation from './card-with-variation.twig';
import './_card-with-variation.scss';

storiesOf('Components|Card with variation', module)
  .add('CardWithVariation', () => CardWithVariation({
    image: Faker.image.imageUrl,
    title: text('Title', Faker.lorem.words(3), 'Content'),
    description: text('Description', Faker.lorem.paragraphs(3), 'Content'),
    link: '#',
    isDark: boolean("Has dark background color?", false, 'Modifiers'),
    hasMedia: boolean("Has image?", true, 'Modifiers'),
  })
);
```

## RESOURCES

### ORGANIZATION
- https://bradfrost.com/blog/post/css-architecture-for-design-systems/

### STORYBOOK / DRUPAL INTEGRATION
- https://medium.com/@askibinski/integrating-storybook-with-drupal-ddabfc6c2f9d
- https://github.com/AmazeeLabs/silverback/tree/master/assets/storybook

### DESIGN TOKENS
- https://dev.to/psqrrl/managing-design-tokens-using-storybook-5975
- https://storybook-design-token.netlify.com/?path=/story/components--button
- https://github.com/UX-and-I/storybook-design-token

### SKETCH
