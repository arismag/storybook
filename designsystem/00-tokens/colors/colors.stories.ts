import { storiesOf } from '@storybook/html';
import { boolean, text } from '@storybook/addon-knobs/html'; // Available knobs: https://www.npmjs.com/package/@storybook/addon-knobs#available-knobs
import Faker from 'faker';

import Colors from './colors.twig';
import './_colors.scss';
// import markdownNotes from './card-with-variation.md';

var colors = require('./colors.json');

const parentColorCategories = colors.baseColors;

for (var category in parentColorCategories) {
  // console.log("Parent category: " + category);
  const parentColorCategory = parentColorCategories[category];
  for (var subCategory in parentColorCategory) {
    console.log("> Sub category: " + subCategory + ": " + parentColorCategory[subCategory]);
    storiesOf(`Tokens|Colors/${category}`, module)
     .add(`${subCategory}`, () => Colors({
       colorValue: parentColorCategory[subCategory],
       colorName: subCategory,
     })
    );
  }
  // console.log("===============================================================");
}
