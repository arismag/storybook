import { storiesOf } from '@storybook/html';
import Faker from 'faker';

import Heading from './heading.twig';

[1, 2, 3, 4, 5, 6].forEach( level =>
    storiesOf('Base|Headings', module)
        .add(`H${level}`, () => Heading({
            level: level,
            text: Faker.lorem.words(6)
        })
    )
);
