_Basic card component for personnel presentation_

**Fields:**

## INTEGRATION

```
{% include '@components/card/card.twig with {
  image: drupal_value;
  name: drupal_value;
  place: drupal_value;
  telephone: drupal_value;
  email: drupal_value;
}}
```
