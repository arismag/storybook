import { storiesOf } from '@storybook/html';
import { boolean, text } from '@storybook/addon-knobs/html'; // Available knobs: https://www.npmjs.com/package/@storybook/addon-knobs#available-knobs
import Faker from 'faker';

import CardWithVariation from './card-with-variation.twig';
import './_card-with-variation.scss';
import markdownNotes from './card-with-variation.md';

storiesOf('Components|Card with variation', module)
  .add('CardWithVariation', () => CardWithVariation({
    image: Faker.image.imageUrl,
    title: text('Title', Faker.lorem.words(3), 'Content'),
    description: text('Description', Faker.lorem.paragraphs(3), 'Content'),
    link: '#',
    isDark: boolean("Has dark background color?", false, 'Modifiers'),
    hasMedia: boolean("Has image?", true, 'Modifiers'),
  }),
    { notes: markdownNotes },
);
