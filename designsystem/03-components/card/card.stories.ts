import { storiesOf } from '@storybook/html';
import Faker from 'faker';

import Card from './card.twig';
import './_card.scss';
import './card.js';
import markdownNotes from './card.md';

storiesOf('Components|Card', module)
  .add('Card', () => Card({
    image: Faker.image.imageUrl,
    title: Faker.lorem.words(3),
    description: Faker.lorem.paragraphs(3),
    link: Faker.internet.url,
  }),
    { notes: markdownNotes },
);
