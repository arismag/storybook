_Basic card component for personnel presentation_


## DESIGN
![card](https://media.nngroup.com/media/editor/2016/10/14/card-wireframe-export-unannotated-v3.png)

**Fields:**

## INTEGRATION

```twig
{% include '@components/card/card.twig with {
  image: drupal_value;
  name: drupal_value;
  place: drupal_value;
  telephone: drupal_value;
  email: drupal_value;
}}
```
