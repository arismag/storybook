// Documentation on theming Storybook: https://storybook.js.org/docs/configurations/theming/

import { create } from '@storybook/theming';

export default create({
  base: 'light',

  brandTitle: 'POINTBLANK',
  brandUrl: 'https://pointblank.gr',
  // brandImage: 'https://placehold.it/350x150', // This works..
  // brandImage: '../logo.png', // Does not work..
});
