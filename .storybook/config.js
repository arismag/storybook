// Addons
import { addDecorator, addParameters, configure } from '@storybook/html';
import { withA11y } from '@storybook/addon-a11y';
import { withKnobs } from '@storybook/addon-knobs/html';
import { DocsPage, DocsContainer } from '@storybook/addon-docs/blocks';
import { setConsoleOptions } from '@storybook/addon-console';

// Theming
import PBtheme from './theme';

addParameters({
  options: {
    theme: PBtheme,
  }
});

// Twig support
import Twig from 'twig';
import twigDrupal from 'twig-drupal-filters';
Twig.cache(); // Clear twig caches. Resolves 'TwigException: There is already a template with the ID' bug. (https://github.com/twigjs/twig.js/issues/414)
twigDrupal(Twig); // Add the filters to Drupal.

// Automatically import all files ending in *.stories.js
const twig = require.context('../designsystem', true, /\.stories\.(ts|js)$/);
function loadStories() {
  twig.keys().sort().forEach(filename => twig(filename));
}

// Helps make UI components more accessible.
addDecorator(withA11y);
addParameters({
  a11y: {
    restoreScroll: true,
  }
});

// Add different background colors.
addParameters({
  backgrounds: [
    { name: 'White', value: '#ffffff' },
    { name: 'Grey', value: '#f5f5f5' },
  ],
});

// Add knobs for variations.
addDecorator(withKnobs);

// Add docs.
addParameters({
  docs: {
    container: DocsContainer,
    page: DocsPage,
  },
});

// Add console.
setConsoleOptions({
  panelExclude: [],
});

configure(loadStories, module);
