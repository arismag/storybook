const path = require('path');

// Import common configurations.
const common = require('../webpack.common');

// Import sass linter.
const SassLintPlugin = require('sass-lint-webpack');

module.exports = ({ config }) => {

  // Support importing typescript files without extension.
  config.resolve.extensions.push('.ts');

  // Add common plugins.
  config.plugins.push(...common.plugins);

  // Add sass linter plugin.
  config.plugins.push(
    new SassLintPlugin()
  );

  // Add js linter plugin.
  config.module.rules.push({
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
        options: {
          emitWarning: true,
        },
      },
  );

  // Reuse common webpack configuration.
  config.module.rules.push(common.javascript);
  config.module.rules.push(common.assets);
  config.module.rules.push(common.css);

  // Twig webpack is only required within storybook.
  config.module.rules.push({
    test: /\.twig$/,
    use: [
      {
        loader: 'twig-loader',
        options: {
          twigOptions: {
            namespaces: {
              storybook: path.resolve(__dirname, '../', 'twig'),
            },
          },
        },
      },
    ],
  });

  // SASS.
  config.module.rules.push({
    test: /\.scss$/,
    use: [
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          sourceMap: true,
        },
      },
      {
        loader: 'sass-loader',
        options: {
          sourceMap: true,
        },
      },
      {
        loader: 'sass-resources-loader',
        options: {
          resources: path.resolve(__dirname, '../designsystem/00-tokens/**/_*.scss')
          // Or array of paths
          // resources: ['./path/to/vars.scss', './path/to/mixins.scss']
        },
      },
    ],
  });

  return config;
};
